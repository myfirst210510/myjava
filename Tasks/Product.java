package Tasks;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class productdetails {
	private String productId;
	private String productName;
	private String description;
	private int price;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}

class orderdetails {
	private String productId;
	private String Name;
	private int price;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}

	class dbcon {
		public List<productdetails> getProductDetails() {
			List<productdetails> product = new ArrayList<productdetails>();

			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/productdetails", "root",
						"5697");
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("select*from productdetails");

				while (rs.next()) {

					productdetails producttetails = new productdetails();

					String pid = rs.getString("productId");
					String pname = rs.getString("productName");
					String pdes = rs.getString("description");
					int pprice = rs.getInt("price");

					producttetails.setProductId(pid);
					producttetails.setProductName(pname);
					producttetails.setDescription(pdes);
					producttetails.setPrice(pprice);

					product.add(producttetails);
				}
			} catch (Exception ex) {
				System.out.println(ex);
			}
			return product;
		}

		public List<orderdetails> getOrderDetails() {
			List<orderdetails> order = new ArrayList<orderdetails>();

			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/productdetails", "root",
						"5697");
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("select*from orderdetails");

				while (rs.next()) {

					orderdetails orderDetails = new orderdetails();

					String pid = rs.getString("productId");
					String pname = rs.getString("name");
					int pprice = rs.getInt("price");

					orderDetails.setProductId(pid);
					orderDetails.setName(pname);
					orderDetails.setPrice(pprice);

					order.add(orderDetails);
				}
			} catch (Exception ex) {
				System.out.println(ex);
			}
			return order;
		}
	}

		public class Product {
			public static void main(String[] args) {

				dbcon dbs = new dbcon();

				List<productdetails> productDe = dbs.getProductDetails();
				List<orderdetails> orderDe = dbs.getOrderDetails();

				Map<String, productdetails> primary = new HashMap<String, productdetails>();

				if (!productDe.isEmpty()) {
					for (productdetails productt : productDe) {
						if (productt.getProductId() != null) {
							primary.put(productt.getProductId(), productt);
						}
					}
				}
				if (!orderDe.isEmpty()) {
					for (orderdetails orderd : orderDe) {
						if (orderd.getProductId() != null) {
							productdetails producc = primary.get(orderd.getProductId());

							System.out.println(producc.getProductId() + " " + producc.getProductName() + " "
									+ producc.getDescription() + " " + producc.getPrice());
						}
					}
				}
			}
		}
	

