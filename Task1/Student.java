package Task1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Students {
	int age;
	String name;

	Students(int age, String name) {
		this.age = age;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Students [age=" + age + ", name=" + name + "]";
	}
}

public class Student implements Comparator {
	public static void main(String[] args) {

		Students stud = new Students(10, "Yuvan");
		Students stud1 = new Students(20, "Vikram");
		Students stud2 = new Students(15, "Ajith");
		Students stud3 = new Students(5, "Kamal");

		List<Students> n = new ArrayList<Students>();
		n.add(stud);
		n.add(stud1);
		n.add(stud2);
		n.add(stud3);

		Collections.sort(n, new Student());
		for (Students sutdd : n) {
			System.out.println(sutdd);
		}
	}
	@Override
	public int compare(Object o1, Object o2) {

		Students s1 = (Students) o1;
		Students s2 = (Students) o2;
		
		//return s1.name.compareTo(s2.name);
		int s3 = s1.name.compareTo(s2.name);
		
		if (s3 == 0) {
			return 0;
		}
		if (s3 > 0) {
			return +1;
		}
		return -1;
	}
}
